package com.conygre.training.tradesimulator.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.conygre.training.tradesimulator.model.User;
import com.conygre.training.tradesimulator.model.Trade;
import com.conygre.training.tradesimulator.model.TradeState.State;

public interface UserMongoDao extends MongoRepository<User, ObjectId>{
	public User findBy_id(ObjectId _id);
}
